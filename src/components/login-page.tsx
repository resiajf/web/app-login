import React from 'react';
import Helmet from 'react-helmet';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { animated, config, Keyframes } from 'react-spring';
import { ToastContainer } from 'react-toastify';

import { Button } from 'src/common/components/bootstrap/buttons';
import { AlternateLogin } from 'src/components/alternate-login';
import { UmaLogin } from 'src/components/uma-login';

import 'react-toastify/dist/ReactToastify.min.css';
import 'src/common/components/app/app.css';
import 'src/styles/login-page.css';

interface LoginPageState {
    showAlternativeLogin: boolean;
}

const delay = (t: number) => new Promise(a => setTimeout(() => a(), t));
const LoginAnimation = Keyframes.Spring({
    close: async (call: any) => {
        await call({ config: config.gentle, to: { x: -180, y: 0 } });
    },
    open: async (call: any) =>{
        await delay(100);
        await call({ config: config.default, to: { x: 0, y: 1 }, });
    },
});

const AlternativeLoginAnimation = Keyframes.Spring({
    close: async (call: any) => {
        await delay(100);
        await call({ config: config.gentle, to: { x: 180, y: 0 } });
    },
    open: {
        config: config.default,
        to: { x: 0, y: 1 },
    },
});

class LoginPageClass extends React.Component<WithNamespaces, LoginPageState> {

    constructor(props: WithNamespaces) {
        super(props);

        this.state = {
            showAlternativeLogin: false,
        };

        this.changeLoginType = this.changeLoginType.bind(this);
    }

    public render() {
        const { t, tReady, i18n } = this.props;

        const centerElement2 = (
            <AlternativeLoginAnimation native={ true } state={ this.state.showAlternativeLogin ? 'open' : 'close' }>
                {({ x, y }: any) =>
                <animated.div className="text-center alternative-login" style={{
                    display: y.interpolate((v: number) => Math.round(v) < 0.01 ? 'none' : 'block'),
                    opacity: y.interpolate((v: number) => `${v}`),
                    transform: x.interpolate((v: number) => `translate(${v}px)`),
                }}>
                    <AlternateLogin t={ t } tReady={ tReady } i18n={ i18n } />
                </animated.div>}
            </AlternativeLoginAnimation>
        );
        const centerElement1 = (
            <LoginAnimation native={ true } state={ this.state.showAlternativeLogin ? 'close' : 'open' }>
                {({ x, y }: any) =>
                <animated.div className="text-center normal-login" style={{
                    display: y.interpolate((v: number) => Math.round(v) < 0.01 ? 'none' : 'block'),
                    opacity: y.interpolate((v: number) => `${v}`),
                    transform: x.interpolate((v: number) => `translate(${v}px)`),
                }}>
                    <UmaLogin t={ t } tReady={ tReady } i18n={ i18n } />
                </animated.div>}
            </LoginAnimation>
        );

        return (
            <div className="login-page">
                <Helmet>
                    <title>{ t('web.title') }</title>
                </Helmet>

                <ToastContainer />

                <div className="title text-center">
                    <h1>{ t('web.title') }</h1>
                </div>
                <div className="content d-flex justify-content-center align-items-center">
                    { centerElement1 }
                    { centerElement2 }
                </div>
                <div className="bottom d-flex justify-content-center align-items-end">
                    <Button type="link" size="sm" onClick={ this.changeLoginType }>
                        { t(this.state.showAlternativeLogin ? 'normal-login' : 'alternative-login') }
                    </Button>
                </div>
            </div>
        );
    }

    private changeLoginType(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            showAlternativeLogin: !this.state.showAlternativeLogin,
        });
    }

}

export default withNamespaces()(LoginPageClass);
