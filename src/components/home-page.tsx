import React from 'react';
import { Helmet } from 'react-helmet';
import { Trans, WithNamespaces, withNamespaces } from 'react-i18next';
import { RouteComponentProps } from 'react-router';
import { ToastContainer } from 'react-toastify';

import Header from 'src/common/components/header';
import Sidebar from 'src/common/components/sidebar';
import { AppStateToProps } from 'src/common/containers/app/interfaces';
import { checkRoutes } from 'src/common/lib/utils';
import { moduleName, routes } from 'src/routes';

import 'react-toastify/dist/ReactToastify.css';
import 'src/common/components/app/app.css';
import asyncComponent from 'src/common/components/async-component';

const AdminLinks = asyncComponent(() => import('./admin-links-home-page'), () => <div className="col-12 col-sm-6 col-xl-3">...</div>);

//Hardcoded :(
const mapModulePageToUrl = {
    bye: {},
    'computer-issues': {
        control: '/computer-issues/admin',
        create: '/computer-issues/create',
        home: '/computer-issues/',
    },
    electricity: {},
    hello: {},
    intos: {
        all: '/intos/',
    },
    netspeed: {
        home: '/netspeed/',
        query: '/netspeed/admin',
    },
    news: {
        home: '/news/',
        stay_connected: '/news/',
    },
    notifications: {
        home: '/notifications/',
        manage: '/notifications/manage',
        past: '/notifications/past',
    },
    voip: {
        home: '/voip/',
        query: '/voip/admin/',
    },

};

interface HomePageState {
    offCanvas: boolean;
}

//See src/common/components/app/app.tsx
class HomePage extends React.Component<AppStateToProps & WithNamespaces & RouteComponentProps<{}>, HomePageState> {

    public constructor(props: any) {
        super(props);
        this.state = {
            offCanvas: false,
        };

        this.toggleOffCanvas = this.toggleOffCanvas.bind(this);

        if(process.env.NODE_ENV !== 'PRODUCTION') {
            //Checks that the routes are valid (only in development)
            checkRoutes(routes);
        }
    }

    public render() {
        const { user, t } = this.props;
        const canSeeThisModule = user.modules.indexOf(moduleName) !== -1;
        const mRoutes = canSeeThisModule ? routes.filter(route => user.pages.indexOf(route.pagePermissionKey) !== -1) : [];

        return (
            <div className="app">

                <Helmet titleTemplate={ `%s - ${this.props.t(`modules.${moduleName}.title`)} | ${this.props.t(`web.title`)}` }
                        defaultTitle={ `${this.props.t(`modules.${moduleName}.title`)} | ${this.props.t(`web.title`)}` } />

                <Header user={ user } onSidebarToggle={ this.toggleOffCanvas } offCanvas={ this.state.offCanvas } />

                <div className="container-fluid">
                    <div className="row flex-xl-nowrap">
                        <Sidebar routes={ mRoutes } modules={ user.modules } offCanvas={ this.state.offCanvas } />

                        <ToastContainer />

                        <main className="col-12 col-md-9 col-xl-10 py-md-3 pl-md-3" role="main">
                            <h1>
                                <Trans i18nKey="welcome.title">
                                    benvenutti {{ name: user.rol === -1 ? t('session.anonymous') : user.displayName }}
                                </Trans>
                            </h1>
                            <p className="lead text-muted">{ t('welcome.subtitle') }</p>

                            <div className="row">
                                <div className="col-12 col-sm-6 col-xl-3">
                                    <h5>Universidad de Málaga</h5>
                                    <ul className="list-unstyled ml-2">
                                        <li><a href="https://campusvirtual.cv.uma.es/" target="_blank">{ t('welcome.cv') }</a></li>
                                        <li><a href="https://www.uma.es/escritorio/" target="_blank">{ t('welcome.escritorio') }</a></li>
                                        <li><a href="https://duma.uma.es/directorio/correoweb/" target="_blank">{ t('welcome.correo') }</a></li>
                                    </ul>
                                </div>
                                { user.rol === 1 && <AdminLinks /> }
                                { user.modules
                                    .sort((a, b) => t(`modules.${a}.title`).localeCompare(t(`modules.${b}.title`)))
                                    .map((moduleNameAgain): [ string, string[] ] => [
                                        moduleNameAgain,
                                        user.pagesPerModule[moduleNameAgain].filter(page => mapModulePageToUrl[moduleNameAgain][page])
                                    ])
                                    .filter(([ _, pages ]) => pages.length > 0)
                                    .map(([ moduleNameAgain, pages ]) => (
                                    <div className="col-12 col-sm-6 col-xl-3" key={ moduleNameAgain }>
                                        <h5>{ t(`modules.${moduleNameAgain}.title`) }</h5>
                                        <ul className="list-unstyled ml-2">
                                            { pages.map(page => (
                                                <li key={ page }><a href={ mapModulePageToUrl[moduleNameAgain][page] }>
                                                    { t(`modules.${moduleNameAgain}.pages.${page}`) }
                                                </a></li>
                                            )) }
                                        </ul>
                                    </div>
                                )) }
                            </div>
                        </main>
                    </div>
                </div>

            </div>
        );
    }

    private toggleOffCanvas(e: any) {
        e.preventDefault();
        this.setState({ offCanvas: !this.state.offCanvas });
    }
}

export default withNamespaces()(HomePage);
