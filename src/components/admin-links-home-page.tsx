import React from 'react';

const URL = process.env.PUBLIC_URL;

export default () => (
    <div className="col-12 col-sm-6 col-xl-3">
        <h5>Enlaces becarios</h5>
        <ul className="list-unstyled ml-2">
            <li><a href={ `${URL}:8443/` } target="_blank">Unifi Control Panel</a></li>
            <li><a href={ `${URL}/netdata1` } target="_blank">netdata (puerta de enlace)</a></li>
            <li><a href={ `${URL}/netdata2` } target="_blank">netdata (otro servidor)</a></li>
            <li><a href={ `${URL}/transmission/web` } target="_blank">transmission</a></li>
        </ul>
    </div>
);