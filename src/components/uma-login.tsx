import React from 'react';
import { WithNamespaces } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { localStorage, sessionStorage } from 'src/common/lib/navigator-storage';

interface UmaLoginState {
    rememberMe: boolean; //yep, that's a reference to a film (again)
}

export class UmaLogin extends React.Component<WithNamespaces, UmaLoginState> {

    constructor(props: WithNamespaces) {
        super(props);

        this.state = {
            rememberMe: localStorage.getItem<boolean>('session-persistent', false)!,
        };

        this.rememberMeChanged = this.rememberMeChanged.bind(this);
        this.loginAsAnonymous = this.loginAsAnonymous.bind(this);
        this.goToUMALogin = this.goToUMALogin.bind(this);
    }

    public render() {
        const { t } = this.props;
        const { rememberMe } = this.state;

        return (
            <form>
                <Button type="success" size="lg" onClick={ this.goToUMALogin } disabled={ true }>{ t('login') }</Button>
                <div className="form-check mt-3">
                    <input type="checkbox"
                           id="keep-session"
                           className="form-check-input"
                           checked={ rememberMe } disabled={ true }
                           onChange={ this.rememberMeChanged } />
                    <label htmlFor="keep-session" className="form-check-label">{ t('keep-session') }</label>
                </div>
                <Button type="warning" outline={ true } size="sm" onClick={ this.loginAsAnonymous } className="mt-3">
                    { t('login-as-anonymous') }
                </Button>
            </form>
        );
    }

    private goToUMALogin(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        localStorage.setItem('session-persistent', this.state.rememberMe);
        window.location.assign('/alberginia/');
    }

    private loginAsAnonymous(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        sessionStorage.setItem('anonymous-login', true);
        window.location.reload();
    }

    private rememberMeChanged(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            rememberMe: e.target.checked
        });
    }

}