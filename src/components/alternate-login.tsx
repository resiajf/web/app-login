import React from 'react';
import { WithNamespaces } from 'react-i18next';
import { toast } from 'react-toastify';

import { Button } from 'src/common/components/bootstrap/buttons';
import { UnauthorizedError } from 'src/common/lib/api';
import { post } from 'src/common/lib/http-client';
import { localStorage } from 'src/common/lib/navigator-storage';
import { Session } from 'src/common/lib/session';

interface AlternateLoginState {
    username: string;
    password: string;
    rememberMe: boolean; //yep, that's a reference to a film
    doingLogin: boolean;
}

export class AlternateLogin extends React.Component<WithNamespaces, AlternateLoginState> {

    constructor(props: WithNamespaces) {
        super(props);

        this.state = {
            doingLogin: false,
            password: '',
            rememberMe: localStorage.getItem<boolean>('session-persistent', false)!,
            username: '',
        };

        this.rememberMeChanged = this.rememberMeChanged.bind(this);
        this.passwordChanged = this.passwordChanged.bind(this);
        this.usernameChanged = this.usernameChanged.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }


    public render() {
        const { doingLogin, username, password, rememberMe } = this.state;
        const { t } = this.props;

        return (
            <form>
                <label htmlFor="inputUsername" className="sr-only">{ t('placeholder.username') }</label>
                <input type="text"
                       id="inputUsername"
                       className="form-control"
                       placeholder={ t('placeholder.username') }
                       disabled={ doingLogin }
                       minLength={ 4 }
                       value={ username }
                       onChange={ this.usernameChanged }
                       required={ true } />
                <label htmlFor="inputPassword" className="sr-only">{ t('placeholder.password') }</label>
                <input type="password"
                       id="inputPassword"
                       className="form-control"
                       placeholder={ t('placeholder.password') }
                       disabled={ doingLogin }
                       value={ password }
                       onChange={ this.passwordChanged }
                       required={ true } />
                <div className="form-check mb-3">
                    <input type="checkbox"
                           id="keep-session2"
                           className="form-check-input"
                           checked={ rememberMe }
                           onChange={ this.rememberMeChanged } />
                    <label htmlFor="keep-session2" className="form-check-label">{ t('keep-session') }</label>
                </div>
                <Button type="success" size="lg" block={ true } onClick={ this.doLogin } disabled={ doingLogin }>{ t('login') }</Button>
            </form>
        );
    }

    private async doLogin(e: React.MouseEvent<HTMLButtonElement>) {
        const { username, password } = this.state;
        if(username.length >= 4 && password) {
            e.preventDefault();
            this.setState({ doingLogin: true });
            try {
                const { token } = await post<
                    { token: string },
                    { username: string, password: string }
                >('alternative-login', { username, password });
                Session.storeToken(token);
                window.location.reload();
            } catch(e) {
                this.setState({ doingLogin: false });
                console.error(e);
                if(e instanceof UnauthorizedError) {
                    toast.error(<div>{ e.translatedMessageKey }</div>);
                }
            }
        }
    }

    private usernameChanged(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            username: e.target.value,
        });
    }

    private passwordChanged(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            password: e.target.value,
        });
    }

    private rememberMeChanged(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            rememberMe: e.target.checked
        });
    }

}