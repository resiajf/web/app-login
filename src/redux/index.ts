import { combineReducers } from 'redux';
import { reducers as commonReducers, State as CommonState } from 'src/common/redux';

export const reducers = combineReducers<State>({
    ...commonReducers,
    //TODO Put some reducers here
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    //TODO Put some state types here
}
