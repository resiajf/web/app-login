import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'login';

const Hello = asyncComponent(() => import('./components/login-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'hello', Hello, 'hello', { exact: true }),
];