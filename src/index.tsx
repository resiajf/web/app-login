import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import asyncComponent from 'src/common/components/async-component';
import ErrorPreLoadingPage from 'src/common/components/error-pre-loading';
import i18n from 'src/common/lib/i18n';
import { sessionStorage } from 'src/common/lib/navigator-storage';
import { Session } from 'src/common/lib/session';
import { User } from 'src/common/redux/user/state';
import { moduleName } from 'src/routes';

const LoginPage = asyncComponent(() => import('src/components/login-page'));
const HomePage = asyncComponent(() => import('src/containers/home-page'));

function defaultPage(user: User) {
    require('bootstrap');

    const composeUpdated = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
    const store = createStore(
        require('./redux').reducers,
        { user }, //Initial state
        composeUpdated(applyMiddleware(thunk))
    );

    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <I18nextProvider i18n={ i18n }>
                    <HomePage />
                </I18nextProvider>
            </BrowserRouter>
        </Provider>,
        document.getElementById('root') as HTMLElement
    );
}

(async () => {
    try {
        const selfUser = await Session.checkSession();
        const user = Session.convertToStatusUser(selfUser, moduleName);

        defaultPage(user);
        //registerServiceWorker();
        //It's not a good idea to enable the service worker in that module, won't let the other modules load as it has
        //enabled a forced redirect to this index.html from browser's URL bar
    } catch(e) {
        if(e === null) {
            if(sessionStorage.getItem('anonymous-login', false)) {
                defaultPage(Session.anonymousUser(moduleName));
            } else {
                ReactDOM.render(
                    <I18nextProvider i18n={i18n}>
                        <LoginPage/>
                    </I18nextProvider>,
                    document.getElementById('root') as HTMLElement
                );
            }
        } else {
            console.error(e);
            ReactDOM.render(
                <ErrorPreLoadingPage error={e}/>,
                document.getElementById('root') as HTMLElement
            );
        }
    }
})();
