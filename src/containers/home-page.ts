import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { AppStateToProps } from 'src/common/containers/app/interfaces';
import { State } from 'src/common/redux/';
import App from 'src/components/home-page';

const mapStateToProps = (state: State): AppStateToProps => ({
    user: state.user
});

// @ts-ignore
export default withRouter(connect(mapStateToProps)(App));
